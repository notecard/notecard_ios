//
//  CardViewController.swift
//  pdfSwitch
//
//  Created by Daniel Schulz on 03/01/2017.
//  Copyright © 2017 Daniel Schulz. All rights reserved.
//

import Foundation
import UIKit



class CardViewController: UIViewController{

    let page:CGPDFPage;
    init(frame: CGRect, card:CGPDFPage)
    {
        page=card;
        super.init(nibName: nil, bundle: nil)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad();
        self.view.frame=super.view.frame;
        self.view.setNeedsLayout();
        self.view.layoutIfNeeded();
        self.view.addSubview(CardView.init(frame: super.view.frame, page: page));
  
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIGraphicsBeginImageContext(view.frame.size)

        UIGraphicsEndImageContext()
    }
    
}
extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static func randomColor() -> UIColor {
        // If you wanted a random alpha, just create another
        // random number for that too.
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
}
