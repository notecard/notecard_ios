//
//  CardSelectionTableViewController.swift
//  pdfSwitch
//
//  Created by Daniel Schulz on 04/01/2017.
//  Copyright © 2017 Daniel Schulz. All rights reserved.
//

import Foundation
import UIKit
import Realm
import RealmSwift


class CardSelectionTableViewController: UITableViewController{
    var courses:[Course] = [];

    
    override func viewDidLoad() {
        let urls=Bundle.main.paths(forResourcesOfType: "pdf", inDirectory: nil)
        let realm = try! Realm()
        for url in urls{
            let file=realm.objects(Course.self).filter("url= '\(url)'");
            var course:Course?=nil;
            if(file.count==0){
                try! realm.write() {
                    course = Course(value: [
                        "url":url,
                        "name":NSURL(fileURLWithPath: url).lastPathComponent!
                    ]);
                    realm.add(course!);
                }
            }else{
                course=file.first!;
            }
            courses.append(course!);
            
            
        }
    }
    
    // MARK: - UITableViewDataSource

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var indexpath = self.tableView.indexPath(for: sender as! UITableViewCell);
        
        // Create a new variable to store the instance of PlayerTableViewController
        let destinationVC = segue.destination as! InterrogationViewController
        destinationVC.course = courses[indexpath!.row];
        // Do your stuff with selectedIndex.row as the index
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return courses.count;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath)
        
        cell.textLabel?.text = courses[indexPath.row].name;
        
        return cell
    }
}
