//
//  Course.swift
//  pdfSwitch
//
//  Created by Daniel Schulz on 13/01/2017.
//  Copyright © 2017 Daniel Schulz. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class Course: Object{
    dynamic var name:String = "";
    let cards = List<Card>();
    dynamic var url:String="";
    
}
