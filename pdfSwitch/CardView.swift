//
//  CardView.swift
//  pdfSwitch
//
//  Created by Daniel Schulz on 04/01/2017.
//  Copyright © 2017 Daniel Schulz. All rights reserved.
//

import Foundation
import UIKit

class CardView: UIView {
    @IBOutlet var view: UIView!
    
    var pageCard:CGPDFPage?;
    
    init(frame: CGRect, page:CGPDFPage) {
        super.init(frame: frame);
        pageCard=page;
        self.frame=super.frame;
        self.isUserInteractionEnabled=false;
        self.contentMode = UIViewContentMode.redraw;
        self.backgroundColor=UIColor.white;
        self.setNeedsLayout();
        self.layoutIfNeeded();
    }

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func draw(_ rect: CGRect) {
        self.frame=super.frame;
        let context = UIGraphicsGetCurrentContext()!
        if let pageCard=pageCard{
            let pageRect: CGRect = pageCard.getBoxRect(CGPDFBox.mediaBox)
            let scale=superview!.superview!.frame.size.width / pageRect.size.width;
            context.saveGState()
            context.translateBy(x: 0.0, y: super.frame.size.height)
            context.scaleBy(x: 1.0, y: -1.0)
            context.scaleBy(x: scale, y: scale)
            context.drawPDFPage(pageCard)
        }
        
        
    }
    
}
