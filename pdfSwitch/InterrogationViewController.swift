//
//  InterrogationViewController.swift
//  pdfSwitch
//
//  Created by Daniel Schulz on 04/01/2017.
//  Copyright © 2017 Daniel Schulz. All rights reserved.
//

import Foundation
import UIKit

class InterrogationViewController: UIViewController{
    @IBOutlet weak var CorrectLabel: UILabel!
    @IBOutlet weak var Button3: UIButton!
    @IBOutlet weak var Button2: UIButton!
    @IBOutlet weak var WrongLabel: UILabel!
    @IBAction func backToMenu(_ sender: Any) {
        dismiss(animated: true, completion: nil);
    }
    @IBOutlet weak var Button1: UIButton!
    var course:Course?=nil;;
    
    override func viewDidLoad() {
        if let viewCtrl=(self.childViewControllers.last){
            let pageViewCtrl=viewCtrl as! CardPageViewController;
            pageViewCtrl.loadPDF(course: course!);
            pageViewCtrl.showNextQuestion();
            displayCardStats(card: pageViewCtrl.displayedCard!);
        }
    }
    
    func questionButtons(){
        Button1.setTitle("Zurück", for: .normal)
        Button2.setTitle("Antwort", for: .normal)
        Button3.setTitle("Überspringen", for: .normal)
        Button1.isEnabled=false;
    }
    
    func answerButtons(){
        Button1.setTitle("Zurück", for: .normal)
        Button2.setTitle("richtig", for: .normal)
        Button3.setTitle("falsch", for: .normal)
        Button1.isEnabled=true;
    }
    
    func displayCardStats(card: Card){
        CorrectLabel.text=String(card.correct);
        WrongLabel.text=String(card.wrong);
    }
    
    @IBAction func clickedButton2(_ sender: UIButton) {

        if let viewCtrl=(self.childViewControllers.last){
            let pageViewCtrl=viewCtrl as! CardPageViewController;
            if pageViewCtrl.displayingAnswer()!{
                pageViewCtrl.answeredCorrect();
                questionButtons()
                displayCardStats(card: pageViewCtrl.displayedCard!);
            }else{
                pageViewCtrl.showAnswer();
                answerButtons()

            }
        }

    }
    @IBAction func clickedButton3(_ sender: UIButton) {
        if let viewCtrl=(self.childViewControllers.last){
            let pageViewCtrl=viewCtrl as! CardPageViewController;
            if pageViewCtrl.displayingAnswer()!{
                pageViewCtrl.answeredWrong();
                questionButtons()
                displayCardStats(card: pageViewCtrl.displayedCard!);
            }else{
                pageViewCtrl.showNextQuestion();
                displayCardStats(card: pageViewCtrl.displayedCard!);
                questionButtons();
                
            }
        }
    }
    @IBAction func clickedButton1(_ sender: UIButton) {
        if let viewCtrl=(self.childViewControllers.last){
            let pageViewCtrl=viewCtrl as! CardPageViewController;
            if pageViewCtrl.displayingAnswer()!{
                questionButtons()
            }else{
                answerButtons()
            }
            pageViewCtrl.goBackCard();
        }

        
    }
    
}
