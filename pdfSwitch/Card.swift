//
//  Card.swift
//  pdfSwitch
//
//  Created by Daniel Schulz on 13/01/2017.
//  Copyright © 2017 Daniel Schulz. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class Card: Object{
    let course = LinkingObjects(fromType: Course.self, property: "cards")
    dynamic var id = 0
    dynamic var cardNr = 0;
    var currentCorrect:Int = 0;
    var currentWrong:Int = 0;
    dynamic var correct:Int = 0;
    dynamic var wrong:Int=0;
    var question:CardViewController?=nil;
    var answer:CardViewController?=nil;
    
    override static func primaryKey() -> String? {
        return "id"
    }

    override static func ignoredProperties() -> [String] {
        return ["question","answer","currentCorrect","currentWrong"]
    }
}
