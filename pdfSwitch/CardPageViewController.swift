//
//  ViewController.swift
//  pdfSwitch
//
//  Created by Daniel Schulz on 09/12/2016.
//  Copyright © 2016 Daniel Schulz. All rights reserved.
//

import UIKit
import CoreFoundation
import CoreGraphics
import RealmSwift;

class CardPageViewController: UIPageViewController {

    var cards:[Card] = [];
    var displayedCard:Card?;
    var previousCard:Card?;
    
    let trainingSetWindow=5;
    let percentageRight=0.75;
    let minTimes=5;

    var trainingSet:[Card] = [];
    
    func getNextCard() ->Card?{
        trainingSet=trainingSet.sorted(by: correctnesRatio);
        cards=cards.sorted(by: correctnesRatio)
        if trainingSet.count<trainingSetWindow{
            fillTrainingSet();
        }
        if let shownCard=displayedCard{
            let cardIndex=trainingSet.index(of: shownCard);
            return trainingSet[(cardIndex!+1)%trainingSet.count];
        }
        displayedCard=trainingSet[0]
        return displayedCard
    }
    func correctnesRatio(_ s1: Card, _ s2: Card) -> Bool {
        //if((s1.wrong==0 && s1.correct==0) || (s2.wrong==0 && s2.correct==0)){
            if((s1.wrong+s1.correct)==(s2.wrong+s2.correct)){
                if(s1.correct == s2.correct){
                    return s1.cardNr<s2.cardNr;
                }else{
                    return (s1.wrong+s1.correct)<(s2.wrong+s2.correct);
                }
            }else{
                return (s1.wrong+s1.correct)<(s2.wrong+s2.correct);
            }
        //}else{
         //   return s1.correct/(s1.correct+s1.wrong)<s2.correct/(s2.correct+s2.wrong)
        //}
    }
    
    func fillTrainingSet(){
        var i=0;
        while(trainingSet.count<trainingSetWindow && i<cards.count){
            if(!trainingSet.contains(cards[i])){
                trainingSet.append(cards[i]);
            }
            i+=1;
        }
    }
    
    public func answeredCorrect(){
        let realm = try! Realm()
        try! realm.write() {
            displayedCard?.correct+=1;
            displayedCard?.currentCorrect+=1;
        }
        
        if(((displayedCard?.wrong)!+(displayedCard?.correct)!)>=minTimes){
            if((displayedCard?.wrong)!>0){
                let correctness:Double=Double(
                    (displayedCard?.correct)!/((displayedCard?.wrong)!))+Double((displayedCard?.correct)!);
                if(correctness>percentageRight){
                    let cardToDelete=displayedCard;
                    let currentPreviousCard = previousCard;
                    showNextQuestion();
                    previousCard=currentPreviousCard;
                    trainingSet.remove(at: trainingSet.index(of: cardToDelete!)!);
                }
            }else{
                let cardToDelete=displayedCard;
                let currentPreviousCard = previousCard;
                showNextQuestion();
                previousCard=currentPreviousCard;
                trainingSet.remove(at: trainingSet.index(of: cardToDelete!)!);
            }
        }else{
            showNextQuestion();
        }
    }
    
    public func answeredWrong(){
        let realm = try! Realm()
        try! realm.write() {
            displayedCard?.wrong+=1;
            displayedCard?.currentWrong+=1;
        }
        showNextQuestion();
    }
    public func showAnswer(){
        self.setViewControllers([displayedCard!.answer!],
                                        direction: .forward,
                                        animated: true,
                                        completion: nil)
    }
    
    public func showNextQuestion(){
        previousCard=displayedCard;//TODO if displayedCard
        displayedCard=getNextCard();
        self.setViewControllers([displayedCard!.question!],
                                direction: .forward,
                                animated: true,
                                completion: nil)
    }
    
    public func goBackCard(){
        let previousCard=pageViewController(self, viewControllerBefore: currentDisplayedViewController()!);
        self.setViewControllers([previousCard!],
                                direction: .reverse,
                                animated: true,
                                completion: nil)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource=self;

    }
    //let chosenCard=Int(arc4random_uniform(UInt32(pageNumber-1))+1)
    public func loadPDF(course:Course){
            let newLoc = NSURL.init(fileURLWithPath: course.url);
            let pdfCfUrl:CFURL = newLoc as CFURL;
            let document=CGPDFDocument(pdfCfUrl as CFURL);
            var i:Int=1;
            let pageNumber=document!.numberOfPages/2;
            let realm = try! Realm()
            print(pageNumber);
            if let pdfDocument=document{
                while(i < pageNumber){
                    let nextId=realm.objects(Card.self).count;
                    var card:Card?=nil;
                    if(course.cards.filter("cardNr=\(i)").count==0){
                        try! realm.write() {
                            card=Card(value: [
                                "id":nextId,
                                "cardNr":i
                            ]);
                            realm.add(card!);
                            course.cards.append(card!);
                        };
                    }
                    if(card==nil){
                        card=course.cards[i-1];
                    }
                    card!.question=CardViewController(frame: self.view.bounds, card:pdfDocument.page(at: card!.cardNr*2-1)!);
                    card!.answer=CardViewController(frame: self.view.bounds, card:pdfDocument.page(at: card!.cardNr*2)!);
                    i += 1;
                    cards.append(card!);
                }
            }
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func currentDisplayedViewController() -> UIViewController? {
            let cardViewCtrl=self.viewControllers!.last as! CardViewController;
            return cardViewCtrl;
        
    }
    


}



// MARK: UIPageViewControllerDataSource

extension CardPageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if(displayedCard?.question?.isEqual(viewController))!{
            if let card = previousCard{
                displayedCard=previousCard;
                previousCard=nil;
                return card.answer;
            }
        }
        if(displayedCard?.answer?.isEqual(viewController))!{
            return displayedCard?.question;
        }
        return nil;
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if(displayedCard?.question?.isEqual(viewController))!{
            return displayedCard?.answer;
        }
        if(displayedCard?.answer?.isEqual(viewController))!{
            previousCard=displayedCard;
            displayedCard=getNextCard();
            return displayedCard?.question;
        }
        return nil;
    }
    func displayingAnswer() -> Bool?{
        if(displayedCard?.question?.isEqual(currentDisplayedViewController()))!{
            return false;
        }
        if(displayedCard?.answer?.isEqual(currentDisplayedViewController()))!{
            return true;
        }
        return nil;

    }
}
